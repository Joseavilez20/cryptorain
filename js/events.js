var last_known_scroll_position = 0;
var ticking = false;
var mainnav = document.getElementById('mainnav')

function doSomething(scroll_pos) {
  // Hacer algo con la posición del scroll
  if(scroll_pos >=50)
  {

  	mainnav.style.backgroundColor = '#221779'
  }else{
  	mainnav.style.backgroundColor = ''
  }
}

window.addEventListener('scroll', function(e) {
  last_known_scroll_position = window.scrollY;
  if (!ticking) {
    window.requestAnimationFrame(function() {
      doSomething(last_known_scroll_position);
      ticking = false;
    });
  }
  ticking = true;
});